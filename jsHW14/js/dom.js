window.onload = function() {
   $('.tabs-title').on('click', function(event) {
      let dataTab = event.target.getAttribute('data-tab');
      $('.tabs-title').each(function() {
         $(this).removeClass('active');
      });
      $(event.target).addClass('active');

      $('.tab-second').each(function(index) {
         if(index == dataTab) {
            $(this).css("display", "block");
         }
         else {
            $(this).css("display", "none");
         }
      });
   });

};